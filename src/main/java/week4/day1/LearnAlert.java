package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlert {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		WebElement frameelement = driver.findElementById("iframeResult");
		driver.switchTo().frame(frameelement);
		driver.findElementByXPath("//button[text()='Try it']").click();
		Alert alert = driver.switchTo().alert();
		String txt = alert.getText();
		System.out.println("Message frm alert: " +txt);
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		alert.dismiss();
		//driver.switchTo().frame("iframeResult");
		String text = driver.findElementById("demo").getText();
		if(text.contains("cancelled"))
		{
			System.out.println("Message Verified");
		}
		driver.switchTo().defaultContent();
		
		
		

	}

}
