package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindows {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);
		
		
		//System.out.println(driver.getTitle());
		
		//switch to secnd window
		
		driver.findElementByPartialLinkText("Contact").click();
		Set<String> allwin = driver.getWindowHandles();
		List<String> ls=new ArrayList<String>(allwin);
		ls.addAll(allwin);
		
		driver.switchTo().window(ls.get(1));
		//driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);
		System.out.println(driver.getTitle());
		driver.switchTo().window(ls.get(0));
		//driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);
		System.out.println(driver.getTitle());
		//close first window
		driver.switchTo().window(ls.get(1)).close();
		driver.switchTo().window(ls.get(0));
		driver.findElementByPartialLinkText("Home").click();
		driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);
		System.out.println(driver.getTitle());
		
		
		
		

	}

}
