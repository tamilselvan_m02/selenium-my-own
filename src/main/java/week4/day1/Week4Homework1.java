package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class Week4Homework1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);
		
		//enterusername
				driver.findElementById("username").sendKeys("DemoSalesManager");
				
				//enterpwd
				driver.findElementById("password").sendKeys("crmsfa");
		
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementByLinkText("Merge Leads").click();
		
		driver.findElementByPartialLinkText("ext-gen").click();
		

	}

}
