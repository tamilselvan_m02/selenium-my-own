package week4.day2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonMethods {
	
	public void allMethods() {
		
		//To launch webdriver
		System.setProperty("webdriver.chromedriver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		//loead URL
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
		
		//dropdown with selectByVisibleText
		
		WebElement elsource = driver.findElementById("");
		Select sel=new Select(elsource);
		//dropdown using selectbyvisibletext
		sel.selectByVisibleText("");
		//dropdown using selectbyvalue
		sel.selectByValue("");
		//dropdown using index
		sel.selectByIndex(0);
		
		//get count of options tag
		List<WebElement> alloptions = sel.getOptions();
		int count = alloptions.size();
		
		//read the text from webelement
		String txt = driver.findElementById("").getText();
		
		//current URL
		String url = driver.getCurrentUrl();
		
		String title = driver.getTitle();
		
		boolean selected = driver.findElementById("").isSelected();
		
		//switch to window
		
		Set<String> allwin = driver.getWindowHandles();
		
		List<String> ls=new ArrayList<>(allwin);
		
		driver.switchTo().window(ls.get(1));
		
		//taking snapshot
		File src = driver.getScreenshotAs(OutputType.FILE);
		
		File dest= new File("./snap/img.png");
		
		try {
			FileUtils.copyFile(src, dest);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//swith to alert
		
		Alert alert = driver.switchTo().alert();
		alert.accept();
		alert.dismiss();
		alert.sendKeys("");
		alert.getText();
		
		//Swith to frames
		
		driver.switchTo().frame(0);
		driver.switchTo().frame("");
		driver.switchTo().frame(driver.findElementById(""));
		driver.switchTo().defaultContent();
		
		driver.switchTo().parentFrame();
		
		//webtable --> 4 row -->3rd column --> read text
		
		WebElement table = driver.findElementById("table1");
		
		List<WebElement> row = table.findElements(By.tagName("tr"));
		List<WebElement> column = row.get(3).findElements(By.tagName("td"));
		System.out.println(column.get(2).getText());
		
		//Waits
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(1000);
		
		WebElement element = driver.findElementById("");
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		
		
		//locators
		
		driver.findElementById("").clear();
		driver.findElementById("").sendKeys("");
		driver.findElementByXPath("").click();
		driver.findElementByClassName("").getAttribute("");
		driver.findElementByLinkText("").getText();
		driver.findElementByCssSelector("").getCssValue("");
		WebElement elename = driver.findElementByName("");
		
		
		
				
	}
		
	}

