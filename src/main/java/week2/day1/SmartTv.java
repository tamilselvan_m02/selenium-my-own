package week2.day1;

public class SmartTv extends Television {
	
	public void enableWifi()
	{
		System.out.println("Wifi Enabled");
	}
	
	public void enableBluetooth()
	{
		System.out.println("Bluetooth Enabled");
	}
	
	public void decreaseVolume()
	{
		System.out.println("Volume key not working");
	}

}
