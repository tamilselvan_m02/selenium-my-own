package week2.day1;

public class PocketRadio extends Radio {
	
	public void changeBattery()
	{
		System.out.println("Battery Changed");
	}
	
	public static void main(String[] args) {
		
		//Radio rad=new Radio();
		PocketRadio pr=new PocketRadio();
		pr.changeBattery();
		pr.volume();
		
	}

}