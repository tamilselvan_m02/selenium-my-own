package week2.day1;

public interface Entertainment {
	
	public void playSongs();
	
	public void playMovies();

}
