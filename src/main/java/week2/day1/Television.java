package week2.day1;

public class Television implements Entertainment {
	
	public void increaseVolume()
	{
		System.out.println("Volume Increased");
	}
	
	public void decreaseVolume()
	{
		System.out.println("Volume Decreased");
	}
	
	public void loudSpeaker()
	{
		System.out.println("Lound Sound");
	}

	@Override
	public void playSongs() {
		// TODO Auto-generated method stub
		
		System.out.println("Songs Played");
	}

	@Override
	public void playMovies() {
		// TODO Auto-generated method stub
		
		System.out.println("Movie Played");
	}

}
