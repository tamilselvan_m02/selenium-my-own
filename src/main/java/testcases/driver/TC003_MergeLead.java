package testcases.driver;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class TC003_MergeLead {

	@Test
	public void mergeLeads() {

		// launch browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		// loadurl
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();

		driver.findElementByXPath("//a[contains(text(),'Merge Leads')]").click();
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();

		Set<String> allwin = driver.getWindowHandles();
		List<String> ls = new ArrayList<String>(allwin);
		ls.addAll(allwin);
		driver.switchTo().window(ls.get(1));
		driver.findElementByXPath("//label[text()='Lead ID:']/following::input").sendKeys("10008");
		driver.findElementByXPath("//button[text()='Find Leads']");
		driver.findElementByLinkText("10008").click();
		System.out.println(ls);
		//driver.switchTo().window(ls.get(0));
		
		/*driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		driver.switchTo().window(ls.get(2));
		driver.findElementByName("id").sendKeys("10011");
		driver.findElementByXPath("//button[text()='Find Leads']");
		driver.findElementByLinkText("10011").click();
		driver.switchTo().window(ls.get(0));
		driver.findElementByLinkText("Merge").click();*/
		
	}

}