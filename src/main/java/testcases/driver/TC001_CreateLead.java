package testcases.driver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class TC001_CreateLead {
	
			//public static void main(String[] args) {
	
	//@Test(invocationCount=2,invocationTimeOut=30000)
	//@Test(invocationCount=2,threadPoolSize=2)
	
	public void createLeads() {
						
			//launch browser
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			ChromeDriver driver=new ChromeDriver();
			driver.manage().window().maximize();
			
			//loadurl
			driver.get("http://leaftaps.com/opentaps/");
			driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
			
			//enterusername pwd
			driver.findElementById("username").sendKeys("DemoSalesManager");
			driver.findElementById("password").sendKeys("crmsfa");
			
			//Click Login and enter values
			
			driver.findElementByClassName("decorativeSubmit").click();
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByLinkText("Leads").click();
			driver.findElementByLinkText("Create Lead").click();
			driver.findElementById("createLeadForm_companyName").sendKeys("Infosys");
			driver.findElementById("createLeadForm_firstName").sendKeys("Tamilselvan");
			driver.findElementById("createLeadForm_lastName").sendKeys("Manoharan");
			driver.findElementByName("submitButton").click();
			driver.close();
	}

}
