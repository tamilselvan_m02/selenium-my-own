package testcases.driver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class TC002_EditLead {

	//public static void main(String[] args) {
	//@Test(dependsOnMethods="testcases.driver.TC001_CreateLead.createLeads")
	@Test
	public void editLeads()
	{
		
		//launch browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		
		//loadurl
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
		
		//enterusername pwd
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		
		//Click Login and enter values
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByName("id");
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Tamilselvan");
		//driver.findElementByXPath(//input[@name='firstName'])[3]"), "Gopinath")
	    //WebElement findEle = driver.findElementByXPath("//label[text()='First name:']/following::input");
	    //findEle.findElement("firstName
	    //driver.findElementByName("firstName").sendKeys("Tamilselvan");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		driver.findElementByLinkText("10111").click();
		String pagetitle = driver.findElementByXPath("//div[text()='View Lead']").getText();
		System.out.println("The page title:" + pagetitle);
		driver.findElementByXPath("//a[text()='Edit']").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("BOFA");
		driver.findElementByXPath("//input[@type='submit']").click();
		String changedname = driver.findElementByXPath("//span[contains(text(),'BOFA')]").getText();
		System.out.println("The changed name is" + changedname);
		
		driver.close();
		
	
		

	}

}
