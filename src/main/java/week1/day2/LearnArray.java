package week1.day2;

import java.util.Scanner;

public class LearnArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Static Array
		//int sal[]= {1,2,3,4};

		/*for(int i=0;i<=3;i++)
		{
			System.out.println(sal[i]);
		}*/
		
		/*for(int i=0;i<sal.length;i++)
		{
			System.out.println(sal[i]);
		}*/
		
		//Dynamic Array
		
		Scanner Scan= new Scanner(System.in);
		//int sal = Scan.nextInt();
		int sal[] = new int[4];
		for(int i=0;i<sal.length;i++)
		{
		     sal[i]=Scan.nextInt();
		     
		}
		
		/*for(int i=0;i<sal.length;i++)
		{
			System.out.println(sal[i]);
		}*/
		//or use for each for display purpose
		
		for(int eachsal :sal)
		{
			System.out.println(eachsal);
		}
		Scan.close();
		
		
}
}



