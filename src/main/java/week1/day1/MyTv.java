package week1.day1;

public class MyTv {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//syntax to call another class
		//ClassName objectName=new ClassName
		
		SmartTv obj1=new SmartTv();
		
		//Syntax to call a method or a Variable
		//ObjectName.MethodName() or ObjectName.Variable()
		obj1.connectWireless("Tamil", "abcd");
		obj1.channelNumber(23);
		int age = obj1.age;
		System.out.println(age);
		String brand = obj1.brand;
		System.out.println(brand);
		int mfgdate = obj1.mfgdate;
		System.out.println(mfgdate);
		int size = obj1.size;
		System.out.println(size);
		

	}

}
